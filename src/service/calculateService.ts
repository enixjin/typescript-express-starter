/**
 * Created by enixjin on 9/5/18.
 */

export class CalculateService {

    add1(a: number, b: number): Promise<number> {
        return Promise.resolve(a + b);
    }

    async add(a: number, b: number): Promise<number> {
        return a + b;
    }
}