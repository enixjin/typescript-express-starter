/**
 * Created by enixjin on 7/18/16.
 */
import {format} from "winston";

let config = require('../config.js');
import * as winston from 'winston';
import * as fs from "fs-extra";

global.config = config;

let fileFormat = format.combine(
    format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.prettyPrint(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);
let consoleFormat = format.combine(
    format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.prettyPrint(),
    format.colorize(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
);
let transports = [];
if (config.logFile) {
    fs.ensureFileSync(config.logFile);
    const files = new winston.transports.File({
        filename: config.logFile,
        format: fileFormat,
        maxsize: 400000,
        maxFiles: 10,
        level: config.logLevelFile ? config.logLevelFile : config.logLevel,
    });
    transports.push(files);

}

const console = new winston.transports.Console({
    format: consoleFormat,
    level: config.logLevel,
    debugStdout: true
});
transports.push(console);

winston.configure({
    transports
});

winston.info("=====================================");
winston.info("==       start service server.     ==");
winston.info("=====================================");
let serviceServer = require("./server");
serviceServer.start();