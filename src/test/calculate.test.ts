/**
 * Created by enixjin on 9/5/18.
 */
import {CalculateService} from "../service/calculateService";

/**
 * Created by enixjin on 8/29/2018.
 */
import * as assert from "assert";

describe("calculate test", function () {
    before(function () {
        // do nothing noew
    });
    describe("Test add", function () {

        it("should return 2 while add 1 to 1", async () => {
            let service = new CalculateService();
            assert.equal(await service.add(1, 1), 2);
        });

        it("should return 5 while add 2 to 3", async () => {
            let service = new CalculateService();
            assert.equal(await service.add(2, 3), 5);
        });

    });
});