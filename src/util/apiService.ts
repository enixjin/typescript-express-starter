/**
 * Created by enixjin on 5/18/16.
 */

import {Subject} from "rxjs";
import * as winston from 'winston';

export class apiService {
    apiSteam = new Subject<api>();

    private static _instance: apiService = new apiService();

    public static getInstance(): apiService {
        return apiService._instance;
    }

    constructor() {
        winston.info("init api service");
        if (apiService._instance) {
            throw new Error("Error: Instantiation failed: Use getInstance() instead of new.");
        }
        apiService._instance = this;

        this.apiSteam.subscribe(
            info => {
                winston.debug(`creating api on URL:{${info.method}}     ${info.url}`);
            }
        );
    }

    push(info: api) {
        this.apiSteam.next(info);
    }

}

export interface api {
    method: string;
    url: string;
}