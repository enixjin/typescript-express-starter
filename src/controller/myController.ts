/**
 * Created by enixjin on 9/5/18.
 */
import {baseController} from "./baseController";
import * as winston from "winston";
import {CalculateService} from "../service/calculateService";

export class MyController extends baseController {

    constructor() {
        super();
        winston.debug(`init MyController`);
        this.initRouters();
    }

    async initRouters() {

        this.get("/", (req, res, next) => {
            res.jsonp({message: "hello world"});
        });

        this.get("/add", async (req, res, next) => {
            let service = new CalculateService();
            let result = await service.add(Number.parseInt(req.query.a), Number.parseInt(req.query.b));
            res.jsonp({result});
        });
    }
}