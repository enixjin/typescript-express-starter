/**
 * Created by Enix on 3/14/2016.
 */
import * as express from "express";
import * as winston from "winston";
import {apiService} from "../util/apiService";

export abstract class baseController {
    router: express.Router;

    constructor() {
        this.router = express.Router();
    }

    //abstract methods
    abstract initRouters();

    public getRouter() {
        return this.router;
    }

    //common get post del put etc.
    get(url: string, callback: (req, res, next) => void) {
        apiService.getInstance().push({method: "get", url: url});
        this.router.get(url, callback);
    }

    post(url: string, callback: (req, res, next) => void, withAuth: boolean = true) {
        this.router.post(url, callback);
    }

    put(url: string, callback: (req, res, next) => void, withAuth: boolean = true) {
        this.router.put(url, callback);
    }

    delete(url: string, callback: (req, res, next) => void) {
        apiService.getInstance().push({method: "delete", url: url});
        this.router.delete(url, callback);
    }

    //utils
    handleError(error: any, res, req?) {
        try {
            res.status(error.statusCode).jsonp(error);
            winston.error(`server error:${JSON.stringify(error)}`);
        } catch (e) {
            winston.error(`unknown server error:${error.stack}`);
            res.status(500).jsonp({
                "errCode": "ERR_500",
                "message": "Server error",
                "statusCode": 500,
                "error": "unknown Server error"
            });
        }
    }

}