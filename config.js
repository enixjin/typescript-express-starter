let config = {};

config.servicePort = 3000;

config.logLevel = 'debug';
config.logFile = '/sandbox/log/server/log.txt';
config.logLevelFile = "debug";

module.exports = config;